# Sistema do BANCO HL

Sisteminha para controle de contas bancárias podendo realizar aberturas, extratos, saques e transferências.

## Funcionalidades

1. Cadastro de Conta
2. Depósitos
3. Saques
4. Transferências
5. Extrato
6. Consulta de Saldo