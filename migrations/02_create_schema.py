# 02_create_schema.py
import sqlite3

# conectando...
conn = sqlite3.connect('database.db')
# definindo um cursor
cursor = conn.cursor()

# criando a tabelas (account, transaction)
cursor.execute("""
CREATE TABLE IF NOT EXISTS accounts(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    email TEXT NOT NULL,
    phone TEXT,
    agency VARCHAR(4) NOT NULL,
    account_number VARCHAR(6) NOT NULL,
    type VARCHAR(1),
    created_at DATE NOT NULL
);
""")

# operation: ['d': 'deposito', 't': 'transferencia', 's': 'saque']
# type: ['d': 'debito', 'c': 'credito']
cursor.execute("""
CREATE TABLE IF NOT EXISTS transactions(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    description TEXT NOT NULL,
    type VARCHAR(1) NOT NULL,
    operation VARCHAR(1) NOT NULL,
    value REAL NOT NULL,
    created_at DATE NOT NULL,
    account_id INTEGER NOT NULL,
    FOREIGN KEY (account_id) REFERENCES accounts (id)
);
""")

print('Tabelas criadas com sucesso.')
# desconectando...
conn.close()