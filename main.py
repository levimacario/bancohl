import os
from termcolor import colored
from modules.menu import Menu
from modules.accounts import Account


def success():
    os.system("clear")
    print(colored("Cadastro realizado com sucesso!", 'green'))

def unavailable():
    os.system('clear')
    print(colored("Função indisponível!", 'yellow'))

def not_found():
    os.system('clear')
    print(colored("Conta bancária não foi encontrada em nosso banco de dados!", 'yellow'))

def invalid():
    os.system('clear')
    print(colored("Opção inválida!", 'yellow'))

def main():
    obj_account = Account()
    obj_menu = Menu()
    
    while True:
        obj_menu.show()
        option = input(colored("Escolha a opção que deseja:", "blue"))
        
        if option == '1':
            account = {'name': '', 'cpf': '', 'email': '', 'phone': ''}

            account['name'] = input(colored("Nome:", 'white'))
            account['cpf'] = input(colored("CPF:", 'white'))
            account['email'] = input(colored("E-mail:", 'white'))
            account['phone'] = input(colored("Celular:", 'white'))
            account['type'] = input(colored("(1)Corrente ou (2)Poupança:", 'white'))

            if not account['name'] or not account['cpf']:
                os.system('clear')
                print(colored("Nome e CPF são obrigatórios para criar nova conta!", 'yellow'))
                continue
            new_account = obj_account.create(account)
            success()
            print(colored("Seus Dados Bancários:", 'white'))
            print(colored("Agência: {0}\nConta: {1}".format(new_account[4], new_account[5]), 'white'))

        elif option == '2':
            agency = input(colored("Agência:", 'white'))
            account_number = input(colored("Conta:", 'white'))
            exists = obj_account.exists(agency, account_number)
            if not exists:
                not_found()
                continue
            value = input(colored("Valor que deseja depositar (Ex.: 120,00): R$ ", 'white'))
            value = value.replace('.', '')
            value = value.replace(',', '.')
            value = float(value)
            deposit = obj_account.make_deposit(agency, account_number, value)
            os.system("clear")
            print(colored("Depósito realizado com sucesso!", 'green'))
            print(colored("Valor depositado: R$ {:06.2f}".format(value), 'white'))
            print(colored("Agência: %s" % agency, 'white'))
            print(colored("Conta: %s" % account_number, 'white'))

        elif option == '3':
            unavailable()

        elif option == '4':
            unavailable()

        elif option == '5':
            unavailable()

        elif option == '6':
            agency = input(colored("Agência:", 'white'))
            account_number = input(colored("Conta:", 'white'))
            exists = obj_account.exists(agency, account_number)
            if not exists:
                not_found()
                continue
            balance = obj_account.balance(agency, account_number)
            os.system("clear")
            print(colored("Consulta realizado com sucesso!", 'green'))
            print(colored("Seu saldo atual é de: R$ {:06.2f}".format(balance), 'white'))
            print(colored("Agência: %s" % agency, 'white'))
            print(colored("Conta: %s" % account_number, 'white'))

        elif option == '7':
            obj_account.list()
            back_to_menu = input("Tecle Enter para voltar ao menu")
            os.system('clear')

        elif option == '8':
            os.system('clear')
            break
        else:
            invalid()

if __name__=='__main__':
    main()