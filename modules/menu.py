from termcolor import colored


class Menu(object):
    '''
    Classe responsável pela exibição
    do menu de opções
    '''
    def show(self):
        self.header
        self.content
        self.footer

    @property
    def header(self):
        print(colored("###############################################", 'white'))
        print(colored("################### Banco HL ##################", 'white'))
        print(colored("###############################################", 'white'))
    
    @property
    def content(self):
        print(colored("1. Criar Conta", 'white'))
        print(colored("2. Realizar Depósito", 'white'))
        print(colored("3. Realizar Saque", 'white'))
        print(colored("4. Transferências", 'white'))
        print(colored("5. Consulta de Extrato", 'white'))
        print(colored("6. Consulta de Saldo", 'white'))
        print(colored("7. Listar Contas", 'white'))
        print(colored("8. Sair", 'white'))
    
    @property
    def footer(self):
        print(colored("\n###############################################", 'white'))