import sqlite3
from random import randint
from termcolor import colored
from datetime import datetime


class Account(object):
    '''
    Classe responsável pelas funções
    relacionadas às contas bancárias.
    '''
    def __init__(self):
        self.agency = '0001'

    def generate_number(self):
        resp = ''
        for i in range(4):
            if i == 0:
                resp += str(randint(1, 9))
            else:
                resp += str(randint(0, 9))
        return resp + '-' + str(randint(1, 9))

    def exists(self, ag, acc_number):
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM accounts WHERE agency='%s' AND account_number='%s';" % (ag, acc_number))
        resp = cursor.fetchall()
        conn.close()
        return len(resp) > 0

    def create(self, account):
        data = [
            (
                account['name'], account['cpf'], account['email'],
                account['phone'], self.agency, self.generate_number(),
                'C' if account['type'] == '1' else 'P',
                datetime.now(),
            ),
        ]
        sql = """INSERT INTO accounts (name, cpf, email, phone, agency, account_number, type, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"""
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.executemany(sql, data)
        conn.commit()
        conn.close()
        return data[0]

    def list(self):
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM accounts;""")

        for acc in cursor.fetchall():
            text = "Nome: {0}  |  CPF: {1}  |  Agência: {2}  |  Conta: {3}  |  Tipo: {4}\n".format(
                acc[1], acc[2], acc[5], acc[6], 'Corrente' if acc[7] == 'C' else 'Poupança'
            )
            print(colored(text, 'white'))
        conn.close()

    def transaction(self, data):
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        sql = """INSERT INTO transactions (description, type, operation, value, created_at, account_id) VALUES (?, ?, ?, ?, ?, ?);"""
        cursor.execute(sql, data)
        conn.commit()
        conn.close()

    def make_deposit(self, ag, acc_number, value):
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.execute("SELECT id FROM accounts WHERE agency='%s' AND account_number='%s';" % (ag, acc_number))
        account_id = cursor.fetchall()[0][0]
        data = ('Depósito', 'C', 'D', value, datetime.now(), account_id)
        self.transaction(data)
        return data

    def balance(self, ag, acc_number):
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.execute("SELECT id FROM accounts WHERE agency='%s' AND account_number='%s';" % (ag, acc_number))
        account_id = cursor.fetchall()[0][0]
        
        cursor.execute("SELECT SUM(value) FROM transactions WHERE account_id=%d;" % account_id)
        balance = cursor.fetchall()[0][0]
        conn.close()

        return balance if balance else 0.00
